package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;


public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> listSpell;
    public ChainSpell(ArrayList<Spell> arrSpell) {
        this.listSpell = arrSpell;
    }

    @Override
    public String spellName() {
        return "Chain Spell";
    }

    @Override
    public void cast() {
        for(Spell spell : listSpell) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for(int i = listSpell.size()-1; i >= 0; i--) {
            listSpell.get(i).cast();
        }
    }

}
